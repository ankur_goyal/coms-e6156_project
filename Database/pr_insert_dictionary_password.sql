USE [SecurityAttacks]
GO
/****** Object:  StoredProcedure [dbo].[pr_insert_dictionary_password]    Script Date: 5/10/2016 6:35:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pr_insert_dictionary_password] 
AS
declare @PasswordCursor CURSOR
declare @PasswordField varchar(100)
declare @salt varchar(100)
declare @entityID int
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @PasswordCursor = CURSOR for
		select CAST(rowguid as varchar(100)), BusinessEntityID from dbo.Dictionary_Person
		order by NEWID()

	open @PasswordCursor 
	fetch next from @PasswordCursor into @PasswordField, @entityID
	while @@FETCH_STATUS = 0
	begin
		set @salt = CAST(NEWID() as varchar(100))
		insert into dbo.dictionary_scene2(Encrypted_Password, Salt, EntityID)
		select CONVERT(VARCHAR(100),HASHBYTES('SHA2_256',@PasswordField + ' ' + @salt),2), @salt, @entityID
		fetch next from @PasswordCursor into @PasswordField, @entityID
	end;
	close @PasswordCursor
	Deallocate @PasswordCursor

END
