USE [SecurityAttacks]
GO
/****** Object:  StoredProcedure [dbo].[pr_xss_alert]    Script Date: 5/10/2016 6:36:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pr_xss_alert] 
	@username varchar(50),
	@data varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    update dbo.sqluser_1 set data=@data where Username=@username

END
