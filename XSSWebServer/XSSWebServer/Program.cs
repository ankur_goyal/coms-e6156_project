﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace XSSWebServer
{
    class Program
    {
        static void Main(string[] args)
        {
            RemoteWebServer ws = new RemoteWebServer(new string[] { "http://localhost:8080/test/" });
            Task.Run(() => ws.Run());
            Console.WriteLine("Web Server has started. Press a key to exit.");
            Console.ReadKey();
            ws.Stop();
        }
    }
}
