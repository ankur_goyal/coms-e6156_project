USE [SecurityAttacks]
GO
/****** Object:  StoredProcedure [dbo].[sp_sql_update]    Script Date: 5/10/2016 6:36:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_sql_update] 
	@username varchar(50),
	@password varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update dbo.sqluser_1 set last_modified=GETDATE() where username=@username
	and password=@password

	select username from dbo.sqluser_1
END
