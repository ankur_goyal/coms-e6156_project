# README #

This document contains the information about the project structure and also contains steps on running the project.

## Project Summary ##
This project will demonstrate the impacts of SQL Injection, XSS scripting and dictionary attacks. It would also display mechanisms which can be implemented in order to prevent/mitigate those attacks.

## Project Structure ##
This project is divided into 3 parts :-

* SecurityAttacks project :- This project contains the UI screens that are displayed to the user. It also contains the code to interact with the database.
* XSSWebServer project :- This project acts as an external Web Server and is used while demonstrating XSS attacks.
* SecurityAttacks.bak / Database directory :- SecurityAttacks.bak is the complete backup of the SQL server database that was used while developing and testing the project. The database directory contains only the SQL scripts for the stored procedures.

## SecurityAttacks project ##
This project contains the following files (apart from the various jQuery libraries) :-

* SecurityAttacks/SecurityAttacks/Controllers/HomeController.cs :- This serves as the primary controller and is responsible for processing requests that are coming from the UI. It contains the following methods :-

1. LoginSQL :- This method is called during demonstrating SQL Injection attack and how it can be prevented. This method is responsible for sending the SQL Injected command to the database.
2. XSSScriptAttack :- This method is called during demonstrating XSS Script attack. This method sends the JavaScript command to the database.
3. XSSValidateLogin :- This method is called while testing the login process in XSS attack. It returns the Javascript command that is saved in the user's entry in the database.
4. LaunchUnsaltedDictionaryAttack :- This method launches a dictionary attack against the unsalted table in the database.
5. GetUnsaltedCount :- This method is used by AJAX calls for getting the number of unsalted records that were impacted by the Dictionary attack.
6. GetSaltedCount :- This method is used by AJAX calls for getting the number of salted records that were impacted by the Dictionary attack.
7. LaunchSaltedDictionaryAttack :- This method launches a dictionary attack against the salted table in the database.
8. GetTableList :- This method is used to populate the table entries that are displayed in the SQL Injection section.

* SecurityAttacks/SecurityAttacks/Models/TableList.cs :- This is a model and is used during displaying table information while demonstrating SQL Injection.

* SecurityAttacks/SecurityAttacks/Models/DBHelper.cs :- This contains the methods that connect to the SQL Server database, fetch relevant information and then parse them. After parsing, that data is returned to the controller.

* SecurityAttacks/SecurityAttacks/Views/Home/_HomePageLayout.cshtml :- This is the master theme for all the web pages. All the web pages display content present in this page.

* SecurityAttacks/SecurityAttacks/Views/Home/SQLIndex.cshtml :- This page functions as the view for demonstrating and preventing SQL Injection. It executes various jQuery operations.

* SecurityAttacks/SecurityAttacks/Views/Home/XSSIndex.cshtml :- This page functions as the view for demonstrating and preventing XSS attacks. It executes various jQuery operations.

* SecurityAttacks/SecurityAttacks/Views/Home/DictionaryAttack.cshtml :- This page functions as the view for demonstrating and mitigating Dictionary attacks. It executes various jQuery operations.

* SecurityAttacks/SecurityAttacks/Web.config :- This is the configuration file for the SecurityAttacks project.

## XSSWebServer project ##
This project contains the following files :-

* XSSWebServer/XSSWebServer/Program.cs :- The Main method launches the web server at the localhost URL and runs the web server on a separate thread.

* XSSWebServer/XSSWebServer/RemoteWebServer.cs :- This is the web server which is launched. It listens to the input requests and prepends them with the phrase :- "This account has cookies:". It then returns the modified string back to the user.

## Pre-Requisite Softwares ##
* Visual Studio 2015
* ASP.NET MVC
* SQL Server 2014

##Running the project ##
1. Checkout the repository
2. Open SQL Server Management Studio and restore the SecurityAttacks database from SecurityAttacks.bak backup.
3. Open two instances of Visual Studio
4. In the first Visual Studio instance, open the XSSWebServer project.
5. In the second Visual Studio instance, open the SecurityAttacks project.
6. Edit the Web.config file of the SecurityAttacks project and enter the SecurityAttacksDB connection string.
7. Run the XSSWebServer and SecurityAttacks project.