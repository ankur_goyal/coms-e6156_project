﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SecurityAttacks.Models
{
    public class DBHelper
    {
        private static readonly Lazy<DBHelper> _instance = new Lazy<DBHelper>(() => new DBHelper());
        private readonly string DBConnectionString;
        public volatile int _UnsaltedDictionaryCount;
        public volatile int _SaltedDictionaryCount;
        private DBHelper()
        {
            DBConnectionString = WebConfigurationManager.ConnectionStrings["SecurityAttacksDB"].ConnectionString;
        }

        public static DBHelper Instance
        {
            get { return _instance.Value; }
        }

        internal bool PlainSQLAttack(string username, string password)
        {
            using (SqlConnection conn = new SqlConnection(DBConnectionString))
            {
                conn.Open();
                string command = "select 1 from dbo.sqluser_1 where username='" + username.ToLower() +
                    "' and password='" + password + "'";
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        return true;
                    }
                }
            }
        }

        internal List<string> ParamSQLPrevention(string username, string password)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(DBConnectionString))
            {
                conn.Open();
                string command = "update dbo.sqluser_1 set last_modified=getdate() where username=@username and password=@password";
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.Parameters.AddWithValue("@password", password);
                    cmd.ExecuteNonQuery();
                }
                command = "select username from dbo.sqluser_1";
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            result.Add(reader["username"].ToString());
                        }
                    }
                }
            }
            return result;
        }

        internal List<string> ProcSQLPrevention(string username, string password)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(DBConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("dbo.sp_sql_update", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.Parameters.AddWithValue("@password", password);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            result.Add(reader["username"].ToString());
                        }
                    }
                }
            }
            return result;
        }

        internal void XSSScriptAttack(string username, string data)
        {
            using (SqlConnection conn = new SqlConnection(DBConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("dbo.pr_xss_alert", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.Parameters.AddWithValue("@data", data);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        internal string XSSTestLogin(string username, string password)
        {
            using (SqlConnection conn = new SqlConnection(DBConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("dbo.pr_get_xss_data", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.Parameters.AddWithValue("@password", password);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            return reader["data"].ToString();
                        }
                    }
                }
            }
            return string.Empty;
        }

        internal void ExecuteUnsaltedDictionaryAttack()
        {
            _UnsaltedDictionaryCount = 0;
            using (SqlConnection conn = new SqlConnection(DBConnectionString))
            {
                conn.Open();
                int limit = 10000;
                int maximum = 1000000;
                int startIdx = 1;
                int endIdx = startIdx + limit;
                while(startIdx <= maximum)
                {
                    using (SqlCommand cmd = new SqlCommand("dbo.pr_dictionary_attack_scene1", conn))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@startIdx", startIdx);
                        cmd.Parameters.AddWithValue("@endIdx", endIdx);
                        cmd.ExecuteNonQuery();
                    }
                    startIdx = endIdx + 1;
                    endIdx = endIdx + limit;
                    _UnsaltedDictionaryCount = _UnsaltedDictionaryCount + limit;
                }
            }
        }

        internal void ExecuteSaltedAttack()
        {
            _SaltedDictionaryCount = 0;
            using (SqlConnection conn = new SqlConnection(DBConnectionString))
            {
                conn.Open();
                // Making the limit small as it is leading to timeouts
                int limit = 100;
                int maximum = 1000000;
                int startIdx = 1;
                int endIdx = startIdx + limit;
                while (startIdx <= maximum)
                {
                    using (SqlCommand cmd = new SqlCommand("dbo.pr_dictionary_attack_scene2", conn))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@startIdx", startIdx);
                        cmd.Parameters.AddWithValue("@endIdx", endIdx);
                        cmd.ExecuteNonQuery();
                    }
                    startIdx = endIdx + 1;
                    endIdx = endIdx + limit;
                    _SaltedDictionaryCount = _SaltedDictionaryCount + limit;
                }
            }
        }


        internal List<TableList> SQLGetTableNames()
        {
            List<TableList> result = new List<TableList>();
            using (SqlConnection conn = new SqlConnection(DBConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("dbo.get_sql_table_list", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            TableList table = new TableList();
                            table.TName = reader["name"].ToString();
                            table.Rows = reader["row_count"].ToString();
                            result.Add(table);
                            table = null;
                        }
                    }
                }
            }
            return result;
        }
    }
}