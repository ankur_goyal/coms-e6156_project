USE [SecurityAttacks]
GO
/****** Object:  StoredProcedure [dbo].[pr_get_xss_data]    Script Date: 5/10/2016 6:35:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pr_get_xss_data] 
	@username varchar(50),
	@password varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select data from dbo.sqluser_1 where Username=@username and Password=@password
END
