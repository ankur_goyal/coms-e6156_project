﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SecurityAttacks.Startup))]
namespace SecurityAttacks
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
