USE [SecurityAttacks]
GO
/****** Object:  StoredProcedure [dbo].[pr_dictionary_attack_scene2]    Script Date: 5/10/2016 6:34:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pr_dictionary_attack_scene2]
	@startIdx int,
	@endIdx int
AS
declare @UserCursor CURSOR
declare @PasswordField varchar(100)
declare @salt varchar(100)
declare @Id  int
declare @PlainPassword varchar(80)
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set @UserCursor = CURSOR for
		select EntityID, Encrypted_Password, Salt from dbo.dictionary_scene2
		where Id between @startIdx and @endIdx

	if @startIdx = 1
	begin
		truncate table dbo.dictionary_scene2_result
	end

	open @UserCursor
	Fetch next from @UserCursor into @Id, @PasswordField, @salt

	While @@FETCH_STATUS = 0
	begin
	Set @PlainPassword = ''
	select @PlainPassword = Plain_Password 
	from dbo.dictionary_table
	where CONVERT(VARCHAR(100),HASHBYTES('SHA2_256',Plain_Password + ' ' + @salt),2) = @PasswordField
	if (@PlainPassword is not null) and (@PlainPassword != '')
	begin
		merge into dbo.dictionary_scene2_result r
		using(select @Id Id, @PlainPassword PlainPassword) y
		on(r.EntityID = y.Id)
		when matched then
			update set Unencrypted_Password=y.PlainPassword
		when not matched then
			insert(EntityID, Unencrypted_Password) values(y.Id, y.PlainPassword);
	end
	Fetch Next from @UserCursor into @Id, @PasswordField, @salt
	end

	Close @UserCursor
	DEALLOCATE @UserCursor

END
