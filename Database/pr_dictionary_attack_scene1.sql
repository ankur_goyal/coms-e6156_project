USE [SecurityAttacks]
GO
/****** Object:  StoredProcedure [dbo].[pr_dictionary_attack_scene1]    Script Date: 5/10/2016 6:34:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pr_dictionary_attack_scene1] 
	@startIdx int,
	@endIdx int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @startIdx = 1
	begin
		truncate table dbo.dictionary_scene1_result
	end
	merge into dbo.dictionary_scene1_result r
	using(select distinct u.EntityID, d.plain_password
	from dbo.dictionary_table d inner join dbo.dictionary_scene1 u
	on (u.id between @startIdx and @endIdx)
	and d.hashed_password = u.encrypted_password) y
	on(r.EntityID = y.EntityID)
	when matched then
		update set unencrypted_password = plain_password
	when not matched then
		insert(EntityID, unencrypted_password) values(y.EntityID, y.plain_password);

END
